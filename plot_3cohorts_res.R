
setwd("/sc/arion/work/garcij62/projects/sczPRSet/result/prset/")

library(VennDiagram)
library(RColorBrewer)

# Read summary files
AYUB_noPT = fread("AYUB-pt_false-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "AYUB"]
AYUB_PT = fread("AYUB-pt_true-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "AYUB"]
Clozuk_noPT = fread("Clozuk_EUR-pt_false-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "Clozuk"]
Clozuk_PT = fread("Clozuk_EUR-pt_true-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "Clozuk"]
Northwell_noPT = fread("Northwell-pt_false-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "Northwell"]
Northwell_PT = fread("Northwell-pt_true-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "Northwell"]
Arango_PT = fread("Arango-pt_true-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "Arango"]
Arango_noPT = fread("Arango-pt_false-gtfNormal.summary") %>%
  .[!Set %like% "Base"] %>%
  .[, Cohort := "Arango"]

cohorts_PT = rbind(AYUB_PT, Clozuk_PT, Arango_PT) 
cohorts_noPT = rbind(AYUB_noPT, Clozuk_noPT, Arango_noPT) 

# Plot Venn diagram for cohorts after PT
myCol <- brewer.pal(3, "Pastel2")
venn.diagram(
  x = list(
    cohorts_PT[Cohort=="AYUB"] %>% select(Set) %>% unlist() , 
    cohorts_PT[Cohort=="Clozuk"] %>% select(Set) %>% unlist() , 
    cohorts_PT[Cohort=="Arango"] %>% select(Set) %>% unlist()
  ),
  category.names = c("AYUB" , "Clozuk" , "Arango"),
  filename = '3cohorts_venn_diagramm.png',
  output=TRUE,
  
  # Output features
  imagetype="png" ,
  height = 480 , 
  width = 480 , 
  resolution = 300,
  compression = "lzw",
  
  # Circles
  lwd = 2,
  lty = 'blank',
  fill = myCol,
  
  # Numbers
  cex = .6,
  fontface = "bold",
  fontfamily = "sans",
  
  # Set names
  cat.cex = 0.6,
  cat.fontface = "bold",
  cat.default.pos = "outer",
  cat.pos = c(-27, 27, 135),
  cat.dist = c(0.055, 0.055, 0.085),
  cat.fontfamily = "sans",
  rotation = 1
)

# Plot R2 across Cohorts (WITH P-value threshold)
ggplot(cohorts_PT, aes(x=as.factor(Set), y=PRS.R2, fill=Cohort, color=Cohort)) + 
  geom_point(aes(size=Num_SNP)) + 
  geom_text_repel(data = cohorts_PT[PRS.R2>0.015],
                  aes(x=as.factor(Set), y=PRS.R2, label=Set), 
                  angle = 0, 
                  size=3,
                  min.segment.length = 0, 
                  segment.size  = 0.1,
                  seed = 42, 
                  box.padding = 0.25) +
  scale_color_manual(values = c("#FF3399", "#009999", "#CCCC00")) +
  scale_fill_manual(values = c("#996699", "#009999", "#CCCC00")) +
  theme_classic() +
  theme(
    axis.text.x = element_blank(),
    axis.text.y = element_text(size = 16),
    legend.text = element_text(size = 16),
    axis.title.x = element_text(size=16),
    axis.title.y = element_text(size=16, vjust=2, hjust=0.5),
    panel.spacing = unit(3, "lines"),
    legend.position = "right") +
  labs(title = c("R2 with P-value Thresholding for genests with Competitive P<0.05"),
       y = expression(paste("Pathway PRS R"^2)),
       x = c("GO terms"))
ggsave("3Cohorts_R2_afterPT.png", width = 12, height = 6)


# Plot R2 across Cohorts (WITHOUT P-value threshold)
color <- c("#9999FF", "#6699CC", "#99CCFF", "#CCCCFF", "#FFCCFF", "#FF99FF", "#FF99CC", "#CC99CC", "#996699", "#FF3399", "#FF6666", "#FF3333", "#00CCCC", "#336666", "#009999", "#003333", "#3366CC", "#0099CC", "#9900FF", "#FF0033", "#CC3300", "#666600", "#CCCC00", "#669900", "#CCCC66")

ggplot(cohorts_noPT, aes(x=as.factor(Set), y=PRS.R2, fill=Cohort, color=Cohort)) + 
  geom_point(aes(size=Num_SNP)) + 
  geom_text_repel(data = cohorts_noPT[PRS.R2>0.015],
                  aes(x=as.factor(Set), y=PRS.R2, label=Set), 
                  angle = 0, 
                  size=3,
                  min.segment.length = 0, 
                  segment.size  = 0.1,
                  seed = 42, 
                  box.padding = 0.25) +
  scale_color_manual(values = c("#FF3399", "#009999", "#CCCC00")) +
  scale_fill_manual(values = c("#996699", "#009999", "#CCCC00")) +
  theme_classic() +
  theme(
    axis.text.x = element_blank(),
    axis.text.y = element_text(size = 16),
    legend.text = element_text(size = 16),
    axis.title.x = element_text(size=16),
    axis.title.y = element_text(size=16, vjust=2, hjust=0.5),
    panel.spacing = unit(3, "lines"),
    legend.position = "right") +
  labs(title = c("R2 without P-value Thresholding and no Competitive.P filter"),
       y = expression(paste("Pathway PRS R"^2)),
       x = c("GO terms"))
ggsave("3Cohorts_R2_beforePT.png", width = 12, height = 6)
