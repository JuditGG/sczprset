
process get_maf_and_pruned_list{
    label 'normal'
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno)
    output:
        tuple   val(meta),
                path("${cohortName}.MAF0.05.bed"),
                path("${cohortName}.MAF0.05.bim"),
                path("${cohortName}.MAF0.05.fam"),
                path(pheno),
                path("${cohortName}.MAF0.05.prune.in")
    script:
    cohortName=bim.baseName
    """
    plink --bfile ${cohortName} \
        --allow-no-sex \
        --indep-pairwise 1000 100 0.2 \
        --maf 0.05 \
        --out ${cohortName}.MAF0.05 \
        --make-bed 
    """
}

process extract_maf_and_pruned_SNPs{
    label 'normal'
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno),
                path(prunedlist)
    output:
        tuple   val(meta),
                path("${cohortName}.LDpruned.bed"),
                path("${cohortName}.LDpruned.bim"),
                path("${cohortName}.LDpruned.fam"),
                path(pheno)
    script:
    cohortName=bim.baseName 
    """
    plink --bfile ${cohortName} \
        --allow-no-sex \
        --extract ${prunedlist} \
        --make-bed \
        --out ${cohortName}.LDpruned
    """
}

process exclude_highIBD_SNPs{
    label 'normal'
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno)
    output:
        tuple   val(meta),
                path("${cohortName}.HighIBDrem.bed"),
                path("${cohortName}.HighIBDrem.bim"),
                path("${cohortName}.HighIBDrem.fam"),
                path(pheno)
    script:
    cohortName=bim.baseName
    """
    plink --bfile ${cohortName} \
        --allow-no-sex \
        --exclude /sc/arion/projects/psychgen/pgc/pgc3seq/data/genotypes/PGC_PLINK/JJ_LMH_QC/FILTERING_FILES/highIBDSNPs \
        --range \
        --make-bed \
        --out ${cohortName}.HighIBDrem
    """
}

process hwe_filter {
    label 'normal'
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno)
    output:
        tuple   val(meta),
                path("${cohortName}_for_PCA.bed"),
                path("${cohortName}_for_PCA.bim"),
                path("${cohortName}_for_PCA.fam"),
                path(pheno)
    script:
    cohortName=bim.baseName 
    """
    plink --bfile ${cohortName} \
        --allow-no-sex \
        --hwe 0.000001 \
        --make-bed \
        --out ${cohortName}_for_PCA
    """
}

process extract_PCs{
    label 'normal'
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno)
    output:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno),
                path("${cohortName}.eigenvec"),
                path("${cohortName}.eigenvec.var"),
                path("${cohortName}.eigenval")
    script:
    cohortName = bim.baseName
    """
    plink --bfile ${cohortName} \
        --allow-no-sex \
        --pca 'var-wts' \
        --out ${cohortName}
    """
}

process generate_phenoPCs_file {
    label 'normal'
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno),
                path(eigenvec),
                path(eigenvec_var),
                path(eigenval)
    output:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path("${name}Pheno_${ancestry}_PCs")
    script:
        if(meta instanceof Map){
        name = meta.name
        ancestry = meta.ancestry
    }
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    library(stringr)

    # Read PCs and rename columns
    pcs = fread("${eigenvec}", colClasses=c(V2="character"))
    colnames(pcs)[3:22] = paste("PC", rep(1:20), sep="")

    # Read phenotype file, select FID, IID, and SCZ (do not select the PCs, as these were )
    pheno = fread("${pheno}") %>%
        .[, c("FID", "IID", "SCZ")]

    if ( "${name}" %like% "Rujescu" | "${name}" %like% "Ressler" | "${name}" %like% "Northwell") {
        pheno = pheno[, FID := str_replace(FID, "[0-9]++_", "")] %>%
        .[, IID := str_replace(IID, "[0-9]++_", "")]
    } else {
        message("No replacement in FID string")
    }

    # Read fam file, where individuals are in the same order as the PCs calculation
    fam = fread("${fam}", colClasses=c(V2="character"))

    merged = merge(fam, pcs, by.x="V2", by.y="V2") %>%
        merge(pheno, ., by.x="FID", by.y="V2") %>%
        .[, c("FID", "IID", "SCZ", paste("PC", rep(1:20), sep=""))] 
    
    print(dim(merged))
    write.table(merged, "${name}Pheno_${ancestry}_PCs", sep="\\t", col.names=T, row.names=F)
    """
}