process edit_bigdeli_GWAS {
    label 'normal'
    input:
        tuple   path(gwas),
                path(map)
    output:
        tuple   val("Bigdeli"),
                path("Bigdeli_gwas")

    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr) 

    gwas = fread("${gwas}")
    map = fread("${map}", h=F)

    gwas_merged = merge(gwas, map, by.x="SNP", by.y="V2")
    setnames(gwas_merged, "SNP", "rsID_GRCh38")
    setnames(gwas_merged, "V1", "SNP")

    fwrite(gwas_merged, "Bigdeli_gwas")
    """
}

process edit_pgc_GWAS {
    label 'normal'
    input:
        path (gwas)
    output:
        tuple   val("PGC2"),
                path("PGCgwas")

    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr) 

    gwas = fread("${gwas}")

    # Change name of columns, so colnames are consistent for Bigdeli and PGC GWAS
    setnames(gwas, "MarkerName", "SNP")
    setnames(gwas, "Allele1", "A1")
    setnames(gwas, "Allele2", "A2")
    setnames(gwas, "P-value", "PVALUE_FE")
    setnames(gwas, "Zscore", "BETA_FE")

    fwrite(gwas, "PGCgwas")
    """
}

process filter_sumstats{
    label 'normal'
     input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno),
                val(gwasName),
                path(gwas)
    output:
        tuple   val(meta),
                val(gwasName),
                path("${gwasName}_MHCexcluded")

    script:
    prefix=bed.baseName
    gwasName=gwas.baseName
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr) 

    bim <- fread("${bim}", header = F)

    xregion <- "chr6:25000000-34000000" %>%
            strsplit(., split = ",") %>%
            unlist


    for (region in xregion) {
        if (region != "") {
            breakdown <- strsplit(region, ":") %>%
                unlist
            range <- breakdown %>%
                tail(n = 1) %>%
                strsplit(., "-") %>%
                unlist %>%
                as.numeric
            chr <- breakdown %>%
                head(n = 1) %>%
                gsub("chr", "", .) %>%
                as.numeric
            # Filter based on bim, as bim always contain all required information
            bim = bim[!(V1 == chr & V4 >= range[1] & V4 <= range[2])]
            }
        }
    
    sumstat <- fread("${gwas}")
    
    sumstat[SNP %in% bim[, V2]] %>%
        .[!duplicated(SNP)] %>%
        fwrite(
            .,
            "${gwasName}_MHCexcluded",
            quote = F,
            na = "NA",
            sep = "\\t"
        )
    """
}