process prset_analysis{
    // This process is responsible for running the PRSet analysis. 
    // If keepBest is false, we will delete the best file, otherwise,
    // we will gzip the best file and provide it as an output
    label "prset"
    afterScript "rm *.prsice"
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno),
                path(gwas),
                path(cov),
                path(gtf),
                val(wind5),
                val(wind3),
                val(perm),
                path(gmt)
    output:
        tuple   val(meta),
                path("${name}-pt_${highRes}-gtf${gtftype}-${gwasName}.summary"), emit: summary
        tuple   val(meta),
                path("${name}-pt_${highRes}-gtf${gtftype}-${gwasName}.best.gz"), emit: best optional true
        tuple   val(meta),
                path("${name}-pt_${highRes}-gtf${gtftype}-${gwasName}.snp"), emit: snp optional true
    stub:
    """
    touch ${name}.summary
    if [[ "${keepBest}" != "false" ]]; then
        touch ${name}.best
    fi
    """
    script:
    if(meta instanceof Map){
        name = meta.name
        ancestry = meta.ancestry
        nPCs = meta.nPCs
        gtftype = meta.gtf
        best = meta.containsKey("keepBest") ? meta.best : "false"
        highRes = meta.containsKey("highRes") ? meta.highRes : "false"
        gwasName = meta.gwasName
    }
    prefix=bed.baseName
    """
    # only do permutation if we are not doing high resolution prset
    highRes="--set-perm ${perm}"
    if [ "${highRes}" == "true" ]; then
        highRes="--upper 0.5"
    fi
    PRSice \
        --base ${gwas} \
        --target ${prefix} \
        --out ${name}-pt_${highRes}-gtf${gtftype}-${gwasName} \
        --keep ${pheno} \
        --pheno ${pheno} \
        --pheno-col SCZ \
        --binary-target T \
        --cov ${cov} \
        --snp SNP \
        --pvalue PVALUE_FE \
        --beta \
        --stat BETA_FE \
        --a1 A1 \
        --a2 A2 \
        --gtf ${gtf} \
        --msigdb ${gmt} \
        --thread ${task.cpus} \
        --ultra \
        --print-snp \
        --maf 0.01 \
        --wind-5 ${wind5}kb \
        --wind-3 ${wind3}kb \
        \${highRes}

    if [[ "${best}" == "false" ]]; then
        rm ${name}-pt_${highRes}-gtf${gtftype}-${gwasName}.best
    else
        gzip *best
    fi
    """
}

process extract_significant_gmt{
    label 'tiny'
    input:
        tuple   val(meta),
                path(summary),
                path(gmt)
    output:
        tuple   val(meta),
                path("trim_gmt")
    script:
    """
    awk 'NR == FNR && FNR > 2 && \$12< 0.05 { a[\$2]=1} NR != FNR && \$1 in a {print}' ${summary} ${gmt} > trim_gmt
    """
}

process modify_prset_result{
    label 'normal'
    publishDir "result/prset_Pt/", mode: 'copy', overwrite: true 
    input:
        tuple   val(meta),
                path(summary)
    output:
            path "${name}-${ancestry}-gtf:${gtftype}-pt:${highRes}_${gwasName}prset.csv"
    script:
    if(meta instanceof Map){
        name = meta.name
        ancestry = meta.ancestry
        nPCs = meta.nPCs
        gtftype = meta.gtf
        best = meta.containsKey("keepBest") ? meta.best : "false"
        highRes = meta.containsKey("highRes") ? meta.highRes : "false"
        gwasName = meta.gwasName
    }
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fread("${summary}") %>%
        .[, Phenotype := NULL] %>%
        .[, Cohort := "${name}"] %>%
        .[, GWAS := "${gwasName}"] %>%
        .[, Software := "PRSet"] %>%
        .[, GTF.Type := "${gtftype}"] %>%
        .[, PvalueThres := "${highRes}"] %>%
        .[, nPCs := "${nPCs}"] %>%
        .[, ancestry := "${ancestry}"] %>%
        fwrite(., "${name}-${ancestry}-gtf:${gtftype}-pt:${highRes}_${gwasName}prset.csv")
    """
}


process prsice_analysis{
    label 'prsice'
    afterScript "rm *.prsice"
    publishDir "result/prs/", mode: 'copy', pattern: "*.gz"
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno),
                path(gwas),
                path(cov)
    output:
        tuple   val(meta),
                path("${name}-${gwasName}.summary"), emit: summary optional true
        tuple   val(meta),
                path("${name}-${gwasName}.best.gz"), emit: best optional true
        tuple   val(meta),
                path("${name}-${gwasName}.all_score.gz"), emit: all optional true
    script:
    if(meta instanceof Map){
        name = meta.name
        ancestry = meta.ancestry
        nPCs = meta.nPCs
        gtftype = meta.gtf
        best = meta.containsKey("keepBest") ? meta.best : "false"
        highRes = meta.containsKey("highRes") ? meta.highRes : "false"
        gwasName = meta.gwasName
    }
    prefix = bed.baseName
    """
    PRSice \
        --base ${gwas} \
        --target ${prefix} \
        --out ${name}-${gwasName} \
        --keep ${pheno} \
        --pheno ${pheno} \
        --pheno-col SCZ \
        --binary-target T \
        --snp SNP \
        --pvalue PVALUE_FE \
        --cov ${cov} \
        --beta \
        --stat BETA_FE \
        --a1 A1 \
        --a2 A2 \
        --thread ${task.cpus} \
        --ultra \
        --print-snp \
        --maf 0.01

    gzip *best
    """
}

process modify_prsice_result{
    label 'normal'
    publishDir "result/prsice/", mode: 'copy', overwrite: true 
    input:
        tuple   val(meta),
                path(summary)
    output:
            path "${name}-${ancestry}-${gwasName}_prsice.csv"
    script:
    if(meta instanceof Map){
        name = meta.name
        ancestry = meta.ancestry
        nPCs = meta.nPCs
        gtftype = meta.gtf
        best = meta.containsKey("keepBest") ? meta.best : "false"
        highRes = meta.containsKey("highRes") ? meta.highRes : "false"
        gwasName = meta.gwasName
    }
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fread("${summary}") %>%
        .[, Phenotype := NULL] %>%
        .[, Cohort := "${name}"] %>%
        .[, GWAS := "${gwasName}"] %>%
        .[, Software := "PRSice"] %>%
        .[, GTF.Type := NA] %>%
        .[, PvalueThres := Threshold] %>%
        .[, nPCs := "${nPCs}"] %>%
        .[, ancestry := "${ancestry}"] %>%
        fwrite(., "${name}-${ancestry}-${gwasName}_prsice.csv")
    """
}