process filter_gmt{
    executor 'local'
    memory '10G'
    time '1h'
    input:
        path("*")
    output:
        path("GeneSet.gmt")
    script:
    """
    cat *.gmt >> result
    grep -v _NULL result > GeneSet.gmt
    """
}

process gene_shift_gtf{
    label 'normal'
    input:
        path(gtf)
        val(extension)
    output:
        tuple   val("Null"),
                path("null.gtf.gz")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    options(scipen=999)
    fread("${gtf}") %>%
      .[V3 == "gene"] %>%
      .[V9 %like% "protein_coding"] %>%
      .[, V4 := as.numeric(V4+${extension})] %>%
      .[, V5 := as.numeric(V5+${extension})] %>%
      unique %>%
      fwrite(., "null.gtf.gz", sep="\\t", na="NA", quote=F)
    """
}

process create_cov_file{
    label 'normal'
    input:
        tuple   val(meta),
                path(bed),
                path(bim),
                path(fam),
                path(pheno)
    output:
        tuple   val(meta),
                path("${cohortName}_${nPCs}PCs.cov")

    script:
    cohortName = meta instanceof Map ? meta.name : "empty"
    nPCs = meta instanceof Map ? meta.nPCs : 0
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    
    pheno = fread("${pheno}")

    # We add +3 because the first three columns are always FID, IID and SCZ
    nCols = ${nPCs}+3

    # Grab PCs, that is columns 4 to the total number of PCs to adjust data for
    pcs = c("FID", "IID", colnames(pheno[,4:nCols]))

    cov = pheno[, ..pcs] %>%
        fwrite("${cohortName}_${nPCs}PCs.cov", sep="\\t")

    """
}