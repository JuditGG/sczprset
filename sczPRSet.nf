#!/usr/bin/env nextflow

////////////////////////////////////////////////////////////////////
//
//  This script is responsible to perform pathwayPRS analyses for scz.
//
////////////////////////////////////////////////////////////////////

nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.2'
timestamp='2022-10-30'
if(params.version) {
    System.out.println("")
    System.out.println("Run Set Analysis - Version: $version ($timestamp)")
    exit 1
}

////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}

// Parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////

include {   edit_bigdeli_GWAS 
            edit_pgc_GWAS
            filter_sumstats   }   from './modules/editGWAS'

include {   create_cov_file
            filter_gmt
            gene_shift_gtf  }   from './modules/dataprep'

include {   get_maf_and_pruned_list
            extract_maf_and_pruned_SNPs
            exclude_highIBD_SNPs
            hwe_filter
            extract_PCs 
            generate_phenoPCs_file  }   from './modules/extract_PCAs'

include {   prset_analysis
            prset_analysis as highResPRSet
            extract_significant_gmt
            modify_prset_result
            modify_prset_result as modify_prsetHighRes_result
            modify_prsice_result
            prsice_analysis  }   from './modules/prs'

include {   combine_files   }   from './modules/misc'
include {   combine_map
            reformat_meta
            reconstruct_meta
            addMeta
            removeMeta  } from './modules/handle_metadata.nf'

////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////

gwas = Channel.fromPath("${params.gwas}")
pgc2gwas = Channel.fromPath("${params.pgc2gwas}")

// Using the JSON file
// Create a channel for each target dataset, containing
// name, PLINK file paths and covariate file path
target = Channel.from(phenoConfig.collect{ content ->
    [   [name: content.name, nPCs: content.nPCs, ancestry: content.ancestry], 
        file(content.bed), 
        file(content.bim),
        file(content.fam),
        file(content.pheno)
    ]})
rsIDmap = Channel.fromPath("${params.rsIDmap}")

// PRSet specific inputs: 
// GMT files
gmt = Channel.fromPath("${params.gmt}/GO.gmt") 
// GTF files
gtf = Channel.fromPath("${params.gtf}")

// Parameter for set based analysis. Default is to remove the MHC region
// and extend each gene region 35kb to 5' and 10kb to 3'
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")

////////////////////////////////////////////////////////////////////
//                  MAIN WORKFLOW
////////////////////////////////////////////////////////////////////

workflow {

    // 1. Generate PCs for each ancestry and cohort
    prepare_input_files()
    // 2. Calculate PRS on  the target datasets
    polygenic_score_analysis(prepare_input_files.out.gwas,
                             prepare_input_files.out.cov)

    // 3. Perform leave one out classification
    //    (From target, one cohort left for validation, rest for training)
}

////////////////////////////////////////////////////////////////////
//                  WORKFLOW DETAIL
////////////////////////////////////////////////////////////////////

workflow prepare_input_files{

    ////////////////////////////////////////////////////////////////
    //                  Prepare input datasets
    ////////////////////////////////////////////////////////////////

    // 1. Use rsID to chr:pos map from Jess to get chr:pos for the GWAS 
    // PLINK function --update-name does not handle duplicated rsIDs
    // nor chr:pos, so I need to remove duplicates from the rsID map
    bigdeliGWAS = gwas \
        | combine(rsIDmap) \
        | edit_bigdeli_GWAS
    
    pgc2GWAS = pgc2gwas \
        | edit_pgc_GWAS

    allgwas = edit_pgc_GWAS.out \
        | mix(bigdeliGWAS)
    
    target \
        | combine(allgwas) \
        | filter_sumstats
    
    // 2. Generate covariates file by subsetting the pheno file 
    // and extracting only the covariates
    target \
        | create_cov_file

    emit:
        gwas = filter_sumstats.out
        cov = create_cov_file.out
}


 workflow polygenic_score_analysis{
    take: gwas
    take: cov

    main:
    ////////////////////////////////////////////////////////////////
    //                  Pathway PRS analysis 
    ////////////////////////////////////////////////////////////////
    
    // Collect GMT file with pathway information and grep -v NULL pathways
    filter_gmt(gmt)
    // Generate frame shifted GTF file as Null
    //    If the signals resides within the genic region, this
    //    shift will allow us to test whether the performance of 
    //    PRSet is due to the genic structure (no change in performance)
    //    or were driven by true signal (significantly decreased performance)
    gtfNorm = Channel.of("Normal") \
      | combine(gtf)
    gene_shift_gtf(gtf, "5000000")
    // This will generate two gtf channels: Normal (without frameshift) and Null (with frameshift) //
    gtfInput = gtfNorm \
        | mix(gene_shift_gtf.out)

    // Combine baseTarget with PRSet specific inputs
    target \
        | combine(cov, by:0) \
        | combine(gwas, by:0) \
        | combine(gtfInput) \
        | map{ a -> def meta = a[0].clone()
                        meta["gwasName"] = a[6]
                        meta["gtf"] = a[8]
                        return([
                            meta,   // meta information
                            a[1],   // bed
                            a[2],   // bim
                            a[3],   // fam
                            a[4],   // pheno 
                            a[7],   // gwas
                            a[5],   // cov
                            a[9]    /* gtf */ ])} \
        | combine(wind5) \
        | combine(wind3) \
        | combine(Channel.of(10000))
        | combine(filter_gmt.out) \
        | prset_analysis 

    prset_res = prset_analysis.out.summary \
        | modify_prset_result 

    ////////////////////////////////////////////////////////////////
    //                  Genome-wide PRS analysis 
    ////////////////////////////////////////////////////////////////

    // Perform PRSice analysis using fast score
    target \
        | combine(cov, by:0) \
        | combine(gwas, by:0) \
        | map{ a -> def meta = a[0].clone()
                        meta["gwasName"] = a[6]
                        return([
                            meta,   // meta information
                            a[1],   // bed
                            a[2],   // bim
                            a[3],   // fam
                            a[4],   // pheno 
                            a[7],   // gwas
                            a[5]    /* cov */ ])} \
        | prsice_analysis

    // Combine outputs
    allPRS_output = prsice_analysis.out.summary \
        | modify_prsice_result \
        | mix(prset_res) \
        | collect

    combine_files("${params.out}.csv", "/sc/arion/work/garcij62/projects/sczPRSet/result/", allPRS_output)
}