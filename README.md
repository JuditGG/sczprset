# Run PRSet for PGC schizophrenia studies

## Overview of the approach

### Analysis plan:
#### DATA
- Pathway dataset: curated GO collection from MSigDB
- GWAS base sample: PGC3+MVP meta-analysis
- Target sample with phenotype and genotype: 7 datasets. 

Jess performed QC for each cohort and ancestry separately. Only extra QC 
that should be done is MAF (Her GWAS done at the 1%) 
1. BioMe SCZ (Freeze 1 and 2). These are combined with Clozuk
2. Northwell
3. Ressler
4. Rujescu
5. Arango
6. Ayub

Only cohort_ancestry samples with N >600 individuals were included for analysis. 

The 7 cohort by ancestry samples used are:
- Clozuk_EUR (fam: BioMe_Clozuk_EUR; N=5,243)
- Northwell_EUR (fam: Northwell_EUR; N=1,390)
- Arango_EUR (fam: arango_span1_eur.hwe0.000001.maf0.001; N=1,038)
- Rujescu_EUR (fam: Rujescu_EUR; N=689)
- AYUB_SAS (fam: AYUB_for_GWAS; N=3,227)
- Northwell_AFR (fam: Northwell_AFR; N=753)
- Ressler_AFR (fam: Ressler_AFR; N=714)

#### MAIN WORKFLOW
1. Run PRSet (and PRSice for comparison) for each cohort and each ancestry separately *without P-value thresholding*. 
P-value threshold set at 1 to extract a list of pathways enriched in GWAS signal based on Competitive P-value (calculated via 10k permutations).

    LEAVING ONE COHORT OUT ANALYSIS:

2. Meta-analysis based on competitive P-values to extract enriched GO terms for each ancestry using metap.
3. Select GO terms with meta-analysis P-value <0.05.
4. Include summary results in GLMNET to apply lasso.
5. Predict in left-out cohort

##### Workflow detail - Nov 2022

###### 1. Edit GWAS 

###### 1.1. Bigdeli GWAS: Add column with chr:pos corresponding to hg38 rsID

    GWAS from Tim was on hg37, but all our datasets are in hg38. Jess got a liftover version of the sumstats in hg38 but SNP names are in rsID (not in chr:position). 
    Adding to Bigdeli original GWAS file info with chr:position from Jess map --> I start with 8997490 (original Bigdeli GWAS), and finish with 4774301

###### 1.2. PGC2 GWAS: Rename colnames to make them consistent with Bigdeli
###### 1.3. Remove MHC region in GWAS file (chr6:25000000-34000000) 
    This is done removing SNPs in bim file, so it is done for each GWAS and each target combination
###### 2. Calculate PCs for each cohort and ancestry separately

    I followed procedure similar to Jess to extract number of PCs

    2.1. Get list of maf>0.05 and pruned SNPs
    2.2. Extract SNPs from list
    2.3. Exclude SNPs with high IBD (list from Jess)
    2.4. Apply HWE filter
    2.5. Extract PCs
    2.6. Generate file with phenotype and PCs information 
    
###### 3. Adjust for the number of PCs that were used for each cohort. 

    I create a covariates file extracting the PCs needed for each cohort
###### 4. Run PRSet. 
    4.1. Run PRSet analyses. I run different versions of this:
        - Standard GO pathways
            - Without P-value thresholding (P=1). This is useful for assessing pathway enrichment (by looking at the competitive P-value)
            - With P-value thresholding only for those pathways with Competitive P < 0.05. This is useful to optimize prediction.

        - PRSet-shift, where I shift GTF file gene boundaries by 5Mb
            - Without P-value thresholding
            - With P-value thresholding
